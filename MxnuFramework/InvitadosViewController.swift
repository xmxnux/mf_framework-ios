//
//  InvitadosViewController.swift
//  MxnuFramework
//
//  Created by Manuel on 29/05/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import UIKit

class InvitadosViewController: MxnuFramework {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("InvitadosViewController")
        loadView(path: "invitados_view")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
