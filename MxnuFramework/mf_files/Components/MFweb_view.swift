//
//  MFweb_view.swift
//  MxnuFramework
//
//  Created by Manuel on 15/04/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
import WebKit
class MFweb_view: ComponentBase{
    private var webView: WKWebView = WKWebView()
    private let webDelegate = WebDelegate()
    required init(context: UIView, jsonView: Dictionary<String, AnyObject>, onCallackClick: OnCallbackClick?, customAdapter: CustomAdapter?) {
        super.init(context: context, jsonView: jsonView, onCallackClick: onCallackClick, customAdapter: customAdapter)
        webView.navigationDelegate = webDelegate
        self.view = webView
        webView.allowsBackForwardNavigationGestures = true
    }
    /*override func viewDidAppear() {
        if let urlD = jsonView["url"] as? String{
            let url = URL(string: urlD)
            print("load page \(url?.absoluteString)")
            (self.view as! WKWebView).load(URLRequest(url: url!))
        }
    }*/
    override func setup() {
        super.setup()
        if let urlD = jsonView["url"] as? String{
            let url = URL(string: urlD)
            (self.view as! WKWebView).load(URLRequest(url: url!))
        }
    }
}
class WebDelegate: NSObject, WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("title = \(webView.title!)")
    }
}
