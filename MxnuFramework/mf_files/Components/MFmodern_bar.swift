//
//  MFmodern_bar.swift
//  MxnuFramework
//
//  Created by Manuel on 5/03/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class MFmodern_bar: ComponentBase{
    required init(context: UIView, jsonView: Dictionary<String, AnyObject>, onCallackClick: OnCallbackClick?, customAdapter: CustomAdapter?) {
        super.init(context: context, jsonView: jsonView, onCallackClick: onCallackClick, customAdapter: customAdapter)
        self.view = UIView()
    }
    override func viewDidAppear() {
        loadViews()
    }
    override func setupLayout() {
        
    }
    func loadViews(){
        let tabBarController = UITabBarController()
        //let tabBarController = ModernBarUIBarTabController()
        tabBarController.modalPresentationStyle = .overFullScreen
        
        var controllers: [UIViewController] = []
        var indexTag: Int = 1
        if let childs = self.jsonView["childs"] as? [Dictionary<String, AnyObject>]{
            for child in childs{
                var image: UIImage? = nil
                let jsonData = try? JSONSerialization.data(withJSONObject: child, options: [])
                let jsonString = String(data: jsonData!, encoding: .utf8)
                let fragment = FrameModernBar()
                if let title = child["title"] as? String{
                    fragment.title = title
                }
                if let icon = child["icon"] as? String{
                    image = UIImage(named: icon)
                    image = resizeImage(image: image!, newWidth: 28)
                }
                if customAdapter != nil{
                    fragment.setCustomAdapter(customAdapter: customAdapter!)
                }
                if onCallackClick != nil{
                    fragment.setOnCallbackClick(onCallbackClick: onCallackClick!)
                }
                fragment.loadView(jsonViewStr: jsonString!)
                fragment.tabBarItem = UITabBarItem(title: fragment.title, image: image, tag: indexTag)
                indexTag += 1
                controllers.append(fragment)
            }
        }
        tabBarController.viewControllers = controllers
        self.getViewController().present(tabBarController, animated: true, completion: nil)
    }
}
func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
    
    let scale = newWidth / image.size.width
    let newHeight = image.size.height * scale
    UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))    
    image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}
