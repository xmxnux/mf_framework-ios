//
//  FrameModernBar.swift
//  MxnuFramework
//
//  Created by Manuel on 5/03/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class FrameModernBar: MxnuFramework{
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.rootView?.viewDidAppear()
    }
}
