//
//  MFinput.swift
//  MxnuFramework
//
//  Created by Manuel on 13/02/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class MFinput: ComponentBase{
    var delegateText: InputDelegate?
    required init(context: UIView, jsonView: Dictionary<String, AnyObject>, onCallackClick: OnCallbackClick?, customAdapter: CustomAdapter?) {
        super.init(context: context, jsonView: jsonView, onCallackClick: onCallackClick, customAdapter: customAdapter)
        let textField: UITextField = UITextField()
        textField.borderStyle = .roundedRect
        delegateText = InputDelegate(textField: textField)
        textField.delegate = delegateText
        self.view = textField
        setup()
    }
    override func getValue() -> String? {
        return (self.view as! UITextField).text
    }
    override func setup() {
        super.setup()
        if let placeholder = jsonView["placeholder"] as? String{
            (self.view as! UITextField).placeholder = placeholder
        }
        if let input_type = jsonView["input_type"] as? String{
            switch input_type {
            case "number":
                (self.view as! UITextField).keyboardType = UIKeyboardType.numberPad
                break
            case "password":
                (self.view as! UITextField).isSecureTextEntry = true
                break
            default:
                break
            }
        }
    }
    
}
