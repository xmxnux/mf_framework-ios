//
//  MFtable.swift
//  MxnuFramework
//
//  Created by Manuel on 1/04/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class MFtable: RequestComponent{
    var tableAdapter: TableAdapter?
    let tableView = UITableView()
    var columns: [Dictionary<String, AnyObject>] = []
    
    required init(context: UIView, jsonView: Dictionary<String, AnyObject>, onCallackClick: OnCallbackClick?, customAdapter: CustomAdapter?) {
        super.init(context: context, jsonView: jsonView, onCallackClick: onCallackClick, customAdapter: customAdapter)
        self.view = UIScrollView()
        setup()
    }
    override func setup() {
        super.setup()
        tableAdapter = TableAdapter(customAdapter: customAdapter, name: name)
        tableView.delegate = tableAdapter
        tableView.dataSource = tableAdapter
        tableView.allowsSelection = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell_table")
        //self.view?.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        //setupTableLayout()
        if let data_container = jsonView["data_container"] as? Dictionary<String, AnyObject>{
            fillMFtable(data_container: data_container)
        }
    }
    
    private func fillMFtable(data_container: Dictionary<String, AnyObject>){
        if let clms = data_container["columns"] as? [Dictionary<String, AnyObject>]{
            columns = clms
        }
        let headersContainer = UIView()
        //headersContainer.backgroundColor = UIColor.brown
        headersContainer.translatesAutoresizingMaskIntoConstraints = false
        var view_ant: UIView? = nil
        var totalWidth: CGFloat = 0
        for i in 0..<columns.count{
            totalWidth += tableAdapter!.DEFAULT_WIDTH
            columns[i]["WIDTH"] = tableAdapter!.DEFAULT_WIDTH as AnyObject
            let headerText = UILabel()
            //headerText.backgroundColor = UIColor.blue
            headerText.text = columns[i]["text"] as? String
            headerText.translatesAutoresizingMaskIntoConstraints = false
            headersContainer.addSubview(headerText)
            headerText.widthAnchor.constraint(equalToConstant: tableAdapter!.DEFAULT_WIDTH).isActive = true
            headerText.topAnchor.constraint(equalTo: headersContainer.topAnchor, constant: 10).isActive = true
            headerText.heightAnchor.constraint(equalToConstant: 40).isActive = true
            if view_ant != nil{
                headerText.leftAnchor.constraint(equalTo: view_ant!.rightAnchor).isActive = true
            }
            view_ant = headerText
        }
        (self.view as! UIScrollView).contentSize.width = totalWidth
        self.view?.addSubview(headersContainer)
        headersContainer.widthAnchor.constraint(equalToConstant: totalWidth).isActive = true
        headersContainer.heightAnchor.constraint(equalToConstant: 45).isActive = true
        headersContainer.topAnchor.constraint(equalTo: self.view!.topAnchor).isActive = true
        
        self.view?.addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: headersContainer.bottomAnchor).isActive = true
        tableView.widthAnchor.constraint(equalToConstant: totalWidth).isActive = true
        //tableView.backgroundColor = UIColor.black
        
        setupTableLayout()
        
        if let data = data_container["data"] as? [Dictionary<String, AnyObject>]{
            loadLocalData(data: data)
        }
        
        if let persistent = data_container["persistent"] as? Dictionary<String, AnyObject>{
            if let name = persistent["name"] as? String{
                if let jsonPersistent = Sessions.read(key: name){
                    if let data = jsonPersistent["data"] as? [Dictionary<String, AnyObject>]{
                        loadLocalData(data: data)
                    }
                }
            }
        }
        
        if let ajax = data_container["ajax"] as? Dictionary<String, AnyObject>{
            if let url = ajax["url"] as? String{
                self.url = url
                if let params = ajax["params"] as? Dictionary<String, AnyObject>{
                    self.params = params
                }
                else{
                    self.params = nil
                }
                if let ajaxBool = ajax["autoLoad"] as? Bool{
                    if ajaxBool{
                        load()
                    }
                }
                else{
                    load()
                }
            }
        }
    }
    
    private func loadLocalData(data: [Dictionary<String, AnyObject>]){
        var tableData: [JsonDataContainer] = []
        var tableHeights: [CGFloat] = []
        for i in data{
            let tmpContainer = JsonDataContainer()
            var maxHeight: CGFloat = 0
            for j in columns{
                var tmpJSON:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
                if let text = i[j["dataIndex"] as! String] as? String{
                    tmpJSON[tableAdapter!.COLUMN_NAME] = text as AnyObject
                    tmpJSON[tableAdapter!.WIDTH] = j[tableAdapter!.WIDTH]
                    //calcule height
                    let column = UILabel(frame: CGRect(x: 0, y: 0, width: tableAdapter!.DEFAULT_WIDTH - 16, height: CGFloat.greatestFiniteMagnitude))
                    column.numberOfLines = 0
                    column.text = text
                    column.lineBreakMode = .byWordWrapping
                    column.sizeToFit()
                    if maxHeight < column.frame.height{
                        maxHeight = column.frame.height
                    }
                }
                tmpContainer.data.append(tmpJSON)
            }
            tableData.append(tmpContainer)
            tableHeights.append(maxHeight)
        }
        tableAdapter!.updateList(list: tableData, heights: tableHeights)
        tableView.reloadData()
        tableView.refreshControl?.endRefreshing()
    }
    
    override func load() {
        let restClient = RestClient()
        let result = ResultTable(tableInstance: self)
        if params != nil{
            restClient.execute(url: self.url!, params: Ajax.getParams(jsonParams: params!, componentBase: self), code: 0, iRestResult: result)
        }
        else{
            restClient.execute(url: self.url!, code: 0, iRestResult: result)
        }
    }
    
    private func setupTableLayout(){
        if let height = jsonView["height"] as? String{
            if height == "match" || height == "wrap"{
                //tableView.topAnchor.constraint(equalTo: self.view!.topAnchor).isActive = true
                tableView.bottomAnchor.constraint(equalTo: self.view!.bottomAnchor).isActive = true
            }
            else{
                if let heightInt = Int(height){
                    tableView.heightAnchor.constraint(equalToConstant: CGFloat(heightInt)).isActive = true
                }
            }
        }
    }
}
class ResultTable: IRestResult{
    var tableInstance: MFtable
    init(tableInstance: MFtable) {
        self.tableInstance = tableInstance
    }
    func onSuccess(code: Int, response: String) {
        print("onSuccess = \(response)")
        var jsonResponse = Dictionary<String, AnyObject>()
        let json = try! JSONSerialization.jsonObject(with: response.data(using: String.Encoding.utf8)!, options: .mutableLeaves)
        if let jsonDict = json as? Dictionary<String, AnyObject>{
            jsonResponse = jsonDict
        }
        if jsonResponse["success"] as! Bool{
            var tableData: [JsonDataContainer] = []
            var tableHeights: [CGFloat] = []
            if let data = jsonResponse["data"] as? [Dictionary<String, AnyObject>]{
                for i in data{
                    let tmpContainer = JsonDataContainer()
                    var maxHeight: CGFloat = 0
                    for j in tableInstance.columns{
                        var tmpJSON:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
                        if let text = i[j["dataIndex"] as! String] as? String{
                            tmpJSON[tableInstance.tableAdapter!.COLUMN_NAME] = text as AnyObject
                            tmpJSON[tableInstance.tableAdapter!.WIDTH] = j[tableInstance.tableAdapter!.WIDTH]
                            //calcule height
                            let column = UILabel(frame: CGRect(x: 0, y: 0, width: tableInstance.tableAdapter!.DEFAULT_WIDTH - 16, height: CGFloat.greatestFiniteMagnitude))
                            column.numberOfLines = 0
                            column.text = text
                            column.lineBreakMode = .byWordWrapping
                            column.sizeToFit()
                            if maxHeight < column.frame.height{
                                maxHeight = column.frame.height
                            }
                        }
                        tmpContainer.data.append(tmpJSON)
                    }
                    tableData.append(tmpContainer)
                    tableHeights.append(maxHeight)
                }
                if let dataContainer = tableInstance.jsonView["data_constainer"] as? Dictionary<String, AnyObject>{
                    if let persistent = dataContainer["persistant"] as? Dictionary<String, AnyObject>{
                        if let name = persistent["name"] as? String{
                            if Sessions.read(key: name) == nil{
                                var data_to_save = Dictionary<String, AnyObject>()
                                data_to_save["data"] = data as AnyObject
                                Sessions.write(key: name, value: data_to_save)
                            }
                        }
                    }
                }
                tableInstance.tableAdapter!.updateList(list: tableData, heights: tableHeights)
                tableInstance.tableView.reloadData()
                tableInstance.tableView.refreshControl?.endRefreshing()
            }
        }
        else{
            
        }
    }
    
    func onError(code: Int, error: String) {
        
    }
}
