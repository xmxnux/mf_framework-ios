//
//  MFlabel.swift
//  MxnuFramework
//
//  Created by Manuel on 4/03/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class MFlabel: ComponentBase{
    required init(context: UIView, jsonView: Dictionary<String, AnyObject>, onCallackClick: OnCallbackClick?, customAdapter: CustomAdapter?) {
        super.init(context: context, jsonView: jsonView, onCallackClick: onCallackClick, customAdapter: customAdapter)
        self.view = UILabel()
        setup()
    }
    override func setup() {
        super.setup()
        if let value = jsonView["value"] as? String{
            //(self.view as! UILabel).text = value
            (self.view as! UILabel).text = MF.parse(params: value, componentBase: self)
        }
        if let textColor = jsonView["textColor"] as? String{
            (self.view as! UILabel).textColor = UIColor.init(hexString: textColor)
        }
    }
}
