//
//  MFbutton.swift
//  MxnuFramework
//
//  Created by Manuel on 17/02/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class MFbutton: ComponentBase{
    required init(context: UIView, jsonView: Dictionary<String, AnyObject>, onCallackClick: OnCallbackClick?, customAdapter: CustomAdapter?) {
        super.init(context: context, jsonView: jsonView, onCallackClick: onCallackClick, customAdapter: customAdapter)
        let button: UIButton = UIButton()
        if let text = self.jsonView["text"] as? String{
            button.setTitle(text, for: .normal)
            let textColor = UIColor.init(hexString: "#0072FE")
            button.setTitleColor(textColor, for: .normal)
        }
        if let textColor = self.jsonView["textColor"] as? String{
            let textColor = UIColor.init(hexString: textColor)
            button.setTitleColor(textColor, for: .normal)
        }
        button.addTarget(self, action: #selector(onClick), for: .touchUpInside)
        self.view = button
    }
    @objc func onClick(){
        print("onClick()")
        if let actions = self.jsonView["actions"] as? [Dictionary<String, AnyObject>]{
            for action in actions{
                MF.doAction(componentBase: self, action: action["action"] as! String, actions: action)
                //doAction(action: action["action"] as! String, actions: action["params"] as! Dictionary<String, AnyObject>)
                if let actionName = action["action"] as? String, actionName == "ajax"{
                    let restClient = RestClient.getInstance()
                    class Result: IRestResult{
                        var buttonInstance: MFbutton
                        var ajaxParms: Dictionary<String, AnyObject>
                        init(buttonInstance: MFbutton, ajaxParams: Dictionary<String, AnyObject>) {
                            self.buttonInstance = buttonInstance
                            self.ajaxParms = ajaxParams
                        }
                        func onSuccess(code: Int, response: String) {
                            print("onSuccess = \(response)")
                            var jsonResponse = Dictionary<String, AnyObject>()
                            let json = try! JSONSerialization.jsonObject(with: response.data(using: String.Encoding.utf8)!, options: .mutableLeaves)
                            if let jsonDict = json as? Dictionary<String, AnyObject>{
                                jsonResponse = jsonDict
                            }
                            if jsonResponse["success"] as! Bool{
                                if let success = self.ajaxParms["success"] as? [Dictionary<String, AnyObject>]{
                                    for action in success{
                                        //(action["params"] as! Dictionary<String, AnyObject>)["response"] = jsonResponse["data"]
                                        if var params = action["params"] as? Dictionary<String, AnyObject>{
                                            params["response"] = jsonResponse["data"]
                                            var tmp = action
                                            tmp["params"] = params as AnyObject
                                            MF.doAction(componentBase: buttonInstance, action: action["action"] as! String, actions: tmp)
                                            //buttonInstance.doAction(action: action["action"] as! String, actions: params)
                                        }
                                    }
                                }
                            }
                            else{
                                if let failed = self.ajaxParms["failed"] as? [Dictionary<String, AnyObject>]{
                                    for action in failed{
                                        if var params = action["params"] as? Dictionary<String, AnyObject>{
                                            params["response"] = jsonResponse["data"]
                                            var tmp = action
                                            tmp["params"] = params as AnyObject
                                            //buttonInstance.doAction(action: action["action"] as! String, actions: params)
                                            MF.doAction(componentBase: buttonInstance, action: action["action"] as! String, actions: tmp)
                                        }
                                    }
                                }
                            }
                        }
                        
                        func onError(code: Int, error: String) {
                            print("onError = \(error)")
                            if let failed = self.ajaxParms["failed"] as? Dictionary<String, AnyObject>{
                                for action in failed{
                                    buttonInstance.doAction(action: action.key, actions: failed)
                                }
                            }
                        }
                    }
                    var ajaxParams = (action["params"] as! Dictionary<String, AnyObject>)
                    let params = Ajax.getParams(jsonParams: ajaxParams["params"] as! Dictionary<String, AnyObject>, componentBase: self)
                    let result = Result(buttonInstance: self, ajaxParams: ajaxParams)
                    result.ajaxParms = ajaxParams
                    restClient.execute(url: (ajaxParams["url"] as! String), params: params, code: 0, iRestResult: result)
                }
            }
        }
    }
    private func doAction(action: String, actions: Dictionary<String, AnyObject>){
        switch action {
        case "alert":
            if let alertText = actions["text"] as? String{
                let ac = UIAlertController(title: "", message: alertText, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.getViewController().present(ac, animated: true, completion: nil)
            }
            break
        case "launch":
            if let activityName = actions["activity"] as? String{
                var instance: UIViewController? = nil
                if let object = NSClassFromString("MxnuFramework.\(activityName)") as? UIViewController.Type{
                    instance = object.init(nibName: nil, bundle: nil)
                }
                else{
                    print("Error instanciando")
                }
                //let controller = LoginViewController()
                if instance != nil{
                    self.getViewController().present(instance!, animated: true, completion: nil)
                }
            }
            break
        case "save-session":
            if let sessions = actions["params"] as? [Dictionary<String, AnyObject>]{
                print("save-session")
                var id: String? = nil
                var data: String? = nil
                for i in sessions{
                    if let tmpId = i["id"] as? String{
                        id = tmpId
                    }
                    if let tmpData = i["data"] as? String{
                        data = tmpData
                    }
                    if id != nil && data != nil && data?.range(of: "$MF$response$getValue") != nil{
                        if let response = actions["response"] as? Dictionary<String, AnyObject>{
                            print("response = \(response)")
                            Sessions.write(key: id!, value: response)
                        }
                    }
                }
            }
            break
        case "clear-session":
            Sessions.clear()
            break
        default:
            break
        }
    }
}
