//
//  MFcombo.swift
//  MxnuFramework
//
//  Created by Manuel on 13/05/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class MFcombo: RequestComponent, IRestResult{
    func onSuccess(code: Int, response: String) {
        print("onSuccess = \(response)")
        var jsonResponse = Dictionary<String, AnyObject>()
        let json = try! JSONSerialization.jsonObject(with: response.data(using: String.Encoding.utf8)!, options: .mutableLeaves)
        if let jsonDict = json as? Dictionary<String, AnyObject>{
            jsonResponse = jsonDict
        }
        if (jsonResponse["success"] as! Bool){
            if let data = jsonResponse["data"] as? [Dictionary<String, AnyObject>]{
                comboAdapter.updateList(list: data)
                (self.view as! UIPickerView).reloadAllComponents()
                if data.count > 0{
                    comboAdapter.actionsOnSelect(row: 0)
                }
            }
        }
        else{
            
        }
    }
    
    func onError(code: Int, error: String) {
        print("Error \(error)")
    }
    
    var comboAdapter = ComboAdapter()
    required init(context: UIView, jsonView: Dictionary<String, AnyObject>, onCallackClick: OnCallbackClick?, customAdapter: CustomAdapter?) {
        super.init(context: context, jsonView: jsonView, onCallackClick: onCallackClick, customAdapter: customAdapter)
        self.view = UIPickerView()
        setup()
    }
    override func setup() {
        super.setup()
        (self.view as! UIPickerView).dataSource = comboAdapter
        (self.view as! UIPickerView).delegate = comboAdapter
        comboAdapter.componentBase = self
        if let events = jsonView["events"] as? Dictionary<String, AnyObject>{
            if let onSelect = events["onSelect"] as? [Dictionary<String, AnyObject>]{
                comboAdapter.jsonEvents = onSelect
            }
        }
        if let data_container = jsonView["data_container"] as? Dictionary<String, AnyObject>{
            fillMFcombo(data_container: data_container)
        }
    }
    func fillMFcombo(data_container: Dictionary<String, AnyObject>){
        let dataContainer =  JsonDataContainer()
        if let fields = data_container["fields"] as? [Dictionary<String, AnyObject>]{
            dataContainer.setFields(fields: fields)
        }
        if let code = data_container["code"] as? String{
            dataContainer.setFieldCode(code: code)
        }
        if let name = data_container["name"] as? String{
            dataContainer.setFieldName(name: name)
        }
        comboAdapter.setDataContainer(dataContainer: dataContainer)
        
        if let data = data_container["data"] as? [Dictionary<String, AnyObject>]{
            comboAdapter.updateList(list: data)
        }
        (self.view as! UIPickerView).reloadAllComponents()
        
        if let ajax = data_container["ajax"] as? Dictionary<String, AnyObject>{
            if let url = ajax["url"] as? String{
                self.url = url
                if let params = ajax["params"] as? Dictionary<String, AnyObject>{
                    self.params = params
                }
                else{
                    self.params = nil
                }
                if let ajaxBool = ajax["autoLoad"] as? Bool{
                    if ajaxBool{
                        load()
                    }
                }
                else{
                    load()
                }
            }
        }
    }
    override func getValue() -> String? {
        return comboAdapter.selectedItem
    }
    override func load() {
        let restClient = RestClient()
        if params != nil{
            restClient.execute(url: self.url!, params: Ajax.getParams(jsonParams: params!, componentBase: self), code: 0, iRestResult: self)
        }
        else{
            restClient.execute(url: self.url!, code: 0, iRestResult: self)
        }
    }
}
