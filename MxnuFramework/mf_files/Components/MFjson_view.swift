//
//  MFjson_view.swift
//  MxnuFramework
//
//  Created by Manuel on 9/03/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class MFjson_view: ComponentBase{
    required init(context: UIView, jsonView: Dictionary<String, AnyObject>, onCallackClick: OnCallbackClick?, customAdapter: CustomAdapter?) {
        super.init(context: context, jsonView: jsonView, onCallackClick: onCallackClick, customAdapter: customAdapter)
        self.view = UIView()
        setup()
    }
}
