//
//  MFlist.swift
//  MxnuFramework
//
//  Created by Manuel on 9/03/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class MFlist: RequestComponent{
    var listAdapter: ListAdapter?
    required init(context: UIView, jsonView: Dictionary<String, AnyObject>, onCallackClick: OnCallbackClick?, customAdapter: CustomAdapter?) {
        super.init(context: context, jsonView: jsonView, onCallackClick: onCallackClick, customAdapter: customAdapter)
        self.view = UITableView()
        print("MFlist")
        setup()
    }
    override func setup() {
        super.setup()
        listAdapter = ListAdapter()
        (self.view as? UITableView)?.delegate = listAdapter
        (self.view as? UITableView)?.dataSource = listAdapter
        (self.view as? UITableView)?.register(UITableViewCell.self, forCellReuseIdentifier: "cell_list")
        if let data_container = jsonView["data_container"] as? Dictionary<String, AnyObject>{
            fillMFlist(data_container: data_container)
        }
    }
    private func fillMFlist(data_container: Dictionary<String, AnyObject>){
        let dataContainer = JsonDataContainer()
        //var dataList: [Dictionary<String, AnyObject>] = []
        if let fields = data_container["fields"] as? [Dictionary<String, AnyObject>]{
            dataContainer.setFields(fields: fields)
        }
        if let code = data_container["code"] as? String{
            dataContainer.setFieldCode(code: code)
        }
        if let name = data_container["name"] as? String{
            dataContainer.setFieldName(name: name)
        }
        listAdapter?.setDataContainer(dataContainer: dataContainer)
        
        if let data = data_container["data"] as? [Dictionary<String, AnyObject>]{
            listAdapter?.updateList(list: data, tableViewInstance: self.view as! UITableView)
        }
        (self.view as! UITableView).reloadData()
        (self.view as! UITableView).refreshControl?.endRefreshing()
        
        if let ajax = data_container["ajax"] as? Dictionary<String, AnyObject>{
            if let url = ajax["url"] as? String{
                self.url = url
                if let params = ajax["params"] as? Dictionary<String, AnyObject>{
                    self.params = params
                }
                else{
                    self.params = nil
                }
                if let ajaxBool = ajax["autoLoad"] as? Bool{
                    if ajaxBool{
                        load()
                    }
                }
                else{
                    load()
                }
            }
        }
    }
    override func load() {
        let restClient = RestClient()
        let result = ResultList(listInstance: self)
        if params != nil{
            restClient.execute(url: self.url!, params: Ajax.getParams(jsonParams: params!, componentBase: self), code: 0, iRestResult: result)
        }
        else{
            restClient.execute(url: self.url!, code: 0, iRestResult: result)
        }
    }
    override func setParams(params: Dictionary<String, AnyObject>) {
        self.params = params
    }
}
class ResultList: IRestResult{
    var listInstance: MFlist
    init(listInstance: MFlist) {
        self.listInstance = listInstance
    }
    func onSuccess(code: Int, response: String) {
        print("onSuccess = \(response)")
        var jsonResponse = Dictionary<String, AnyObject>()
        let json = try! JSONSerialization.jsonObject(with: response.data(using: String.Encoding.utf8)!, options: .mutableLeaves)
        if let jsonDict = json as? Dictionary<String, AnyObject>{
            jsonResponse = jsonDict
        }
        if jsonResponse["success"] as! Bool{
            if let data = jsonResponse["data"] as? [Dictionary<String, AnyObject>]{
                listInstance.listAdapter?.updateList(list: data, tableViewInstance: listInstance.view as! UITableView)
                //(listInstance.view as! UITableView).reloadData()
                //(listInstance.view as! UITableView).refreshControl?.endRefreshing()
            }
        }
        else{
            
        }
    }
    
    func onError(code: Int, error: String) {
        
    }
}
