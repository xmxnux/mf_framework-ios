//
//  CloseableInput.swift
//  MxnuFramework
//
//  Created by Manuel on 15/05/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class InputDelegate: NSObject, UITextFieldDelegate{
    var textField: UITextField
    init(textField: UITextField){
        self.textField = textField
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn")
        //textField.endEditing(true)
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("textFieldShouldBeginEditing")
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("shouldChangeCharactersIn")
        return true
    }
    override func didChangeValue(forKey key: String) {
        print("didChangeValue")
    }
}
