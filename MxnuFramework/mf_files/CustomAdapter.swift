//
//  CustomAdapter.swift
//  MxnuFramework
//
//  Created by Manuel on 14/05/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
protocol CustomAdapter{
    func  onBind(name: String, holder: UIView, position: Int, type: Int)
}
