//
//  Ajax.swift
//  MxnuFramework
//
//  Created by Manuel on 5/03/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
class Ajax{
    static func getParams(jsonParams: Dictionary<String, AnyObject>, componentBase: ComponentBase) -> String{
        print("getParams")
        var params: String = ""
        var first = true
        for paramsIterator in jsonParams{
            let key = paramsIterator.key
            let param = paramsIterator.value as! String
            var values = param.split(separator: "$")
            var response: String? = ""
            if values.count == 2{
                let currentInstance = (componentBase.getRoot() as! LayoutBasic).down(name: String(values[0]))
                if currentInstance != nil{
                    response = currentInstance!.getValue()!
                }
                else{
                    response = ""
                }
            }
            else if values.count == 1{
                response = String(values[0])
            }
            else if values.count >= 3{
                if param.range(of: "$MF$Session$") != nil{
                    var tmp = Sessions.read(key: String(values[2]))
                    response = tmp?[String(values[3])] as? String
                }
            }
            print("\(values)")
            response = response?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            if first{
                first = false
            }
            else{
                params += "&"
            }
            params += key + "=" + response!
        }
        print("params = \(params)")
        return params
    }
}
