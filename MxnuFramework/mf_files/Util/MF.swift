//
//  MF.swift
//  MxnuFramework
//
//  Created by Manuel on 15/04/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class MF{
    class UpdateAppSourceHandler: IRestResult{
        func onSuccess(code: Int, response: String) {
            do{
                let json = try JSONSerialization.jsonObject(with: response.data(using: String.Encoding.utf8)!, options: .mutableLeaves)
                var updateError = false
                if let jsonDict = json as? Dictionary<String, AnyObject>{
                    if let views = jsonDict["views"] as? [Dictionary<String, AnyObject>], let appVersion = jsonDict["appVersion"] as? String{
                        var viewToSave = Dictionary<String,AnyObject>()
                        for view in views{
                            if let viewPath = view["file"] as? String, let viewText = view["value"] as? String{
                                viewToSave[viewPath] = viewText as AnyObject
                                //Sessions.write(key: "views", value: jsonDict)
                                //if !Util.writeFile(path: viewPath, content: viewText){
                                    //updateError = true
                                //}
                            }
                            else{
                                updateError = true
                            }
                        }
                        if !updateError {
                            Sessions.write(key: "views", value: viewToSave)
                            var dictAppVersion = Dictionary<String, AnyObject>()
                            dictAppVersion["value"] = appVersion as AnyObject
                            Sessions.write(key: "appVersion", value: dictAppVersion)
                            if let sharedView = MxnuFramework.sharedView{
                                var alertParams = Dictionary<String,AnyObject>()
                                var params = Dictionary<String,AnyObject>()
                                params["text"] = "La aplicacion se actualizó" as AnyObject
                                alertParams["params"] = params as AnyObject
                                MF.doAction(componentBase: sharedView, action: "alert", actions: alertParams)
                                print("sharedView != nil")
                            }
                        }
                    }
                }
                
            }
            catch{
                print("UPDATE: json format invalid")
            }
        }
        
        func onError(code: Int, error: String) {
            print(error)
        }
        
        
    }
    class CheckVersionHandler: IRestResult{
        func onSuccess(code: Int, response: String) {
            var appVersion = "1.0"
            if let localVersion = Sessions.read(key: "appVersion"){
                if let value = localVersion["value"] as? String{
                    appVersion = value
                }
            }
            do{
                print(response)
                let json = try JSONSerialization.jsonObject(with: response.data(using: String.Encoding.utf8)!, options: .mutableLeaves)
                print(json)
                if let jsonDict = json as? Dictionary<String, AnyObject>{
                    if let serverVersion = jsonDict["version"] as? String, let urlUpdateSource = jsonDict["urlUpdateSource"] as? String, serverVersion != appVersion{
                        print("update required")
                        if let sharedView = MxnuFramework.sharedView{
                            var alertParams = Dictionary<String,AnyObject>()
                            var params = Dictionary<String,AnyObject>()
                            params["text"] = "La aplicacion se está actualizando" as AnyObject
                            alertParams["params"] = params as AnyObject
                            MF.doAction(componentBase: sharedView, action: "alert", actions: alertParams)
                            print("sharedView != nil")
                        }
                        let updateAppSourceHandler = UpdateAppSourceHandler()
                        let updateAppSourceClient = RestClient()
                        updateAppSourceClient.execute(url: urlUpdateSource, code: 2, iRestResult: updateAppSourceHandler)
                    }
                }
            }
            catch{
                print("CHECK VERSION: json format invalid")
            }
        }
        
        func onError(code: Int, error: String) {
            print(error)
        }
        
        
    }
    static func updateApp(urlCheckVersion: String){
        let checkVersionHandler = CheckVersionHandler()
        let checkVersionClient = RestClient()
        checkVersionClient.execute(url: urlCheckVersion, code: 1, iRestResult: checkVersionHandler)
    }
    static func parse(params: String, componentBase: ComponentBase) -> String{
        var returnValue: String = params
        let values = params.split(separator: "$")
        var response = String()
        
        if values.count == 2{
            let currentInstance: ComponentBase = (componentBase.getRoot() as! LayoutBasic).down(name: String(values[0]))!
            response = currentInstance.getValue()!
        }
        else if values.count == 1{
            response = String(values[0])
        }
        else if values.count >= 3{
            if params.range(of: "$MF$Session$") != nil{
                var tmp = Sessions.read(key: String(values[2]))
                response = tmp?[String(values[3])] as! String
            }
        }
        print("\(values)")
        returnValue = response
        
        return returnValue
    }
    static func doAction(componentBase: ComponentBase, action: String, actions: Dictionary<String, AnyObject>){
        print("doAction = \(action)")
        switch action {
        case "clear-session":
            if let params = actions["params"] as? Dictionary<String, AnyObject>{
                if let key = params["key"] as? String{
                    print("clear session1 \(key)")
                    Sessions.clear(key: key)
                    print("clear session2 \(key)")
                }
            }
            else{
                Sessions.clear()
            }
            break
        case "save-session":
            if let params = actions["params"] as? Dictionary<String, AnyObject>{
                if let sessions = params["params"] as? [Dictionary<String, AnyObject>]{
                    print("save-session")
                    var id: String? = nil
                    var data: String? = nil
                    for i in sessions{
                        if let tmpId = i["id"] as? String{
                            id = tmpId
                        }
                        if let tmpData = i["data"] as? String{
                            data = tmpData
                        }
                        if id != nil && data != nil && data?.range(of: "$MF$response$getValue") != nil{
                            if let response = params["response"] as? Dictionary<String, AnyObject>{
                                print("response = \(response)")
                                Sessions.write(key: id!, value: response)
                            }
                        }
                    }
                }
            }
            break
        case "alert":
            if let params = actions["params"] as? Dictionary<String, AnyObject>{
                if let alertText = params["text"] as? String{
                    let ac = UIAlertController(title: "", message: alertText, preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    componentBase.getViewController().present(ac, animated: true, completion: nil)
                }
            }
            break
        case "launch":
            if let params = actions["params"] as? Dictionary<String, AnyObject>{
                if let activityName = params["activity"] as? String{
                    var instance: UIViewController? = nil
                    if let object = NSClassFromString("MxnuFramework.\(activityName)") as? UIViewController.Type{
                        instance = object.init(nibName: nil, bundle: nil)
                    }
                    else{
                        print("Error instanciando")
                    }
                    //let controller = LoginViewController()
                    if instance != nil{
                        componentBase.getViewController().present(instance!, animated: true, completion: nil)
                    }
                }
            }
            break
        case "selector":
            if let params = actions["params"] as? Dictionary<String, AnyObject>{
                let cb: ComponentBase = (componentBase.getRoot() as! LayoutBasic).down(name: params["name"] as! String)!
                (cb as! RequestComponent).setParams(params: params["params"] as! Dictionary<String, AnyObject>)
                (cb as! RequestComponent).load()
            }
            break
        default:
            break
        }
    }
}
