//
//  Sessions.swift
//  MxnuFramework
//
//  Created by Manuel on 15/03/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
class Sessions{
    static func write(key: String, value: Dictionary<String, AnyObject>){
        let session = UserDefaults.standard
        session.set(value, forKey: key)
        print("session = \(session)")
    }
    static func read(key: String) -> Dictionary<String, AnyObject>?{
        let session = UserDefaults.standard
        let value = session.object(forKey: key) as? Dictionary<String, AnyObject>
        return value
    }
    static func clear(key: String){
        let session = UserDefaults.standard
        session.removeObject(forKey: key)
    }
    static func clear(){
        let session = UserDefaults.standard
        for i in session.dictionaryRepresentation(){
            session.removeObject(forKey: i.key)
        }
    }
}
