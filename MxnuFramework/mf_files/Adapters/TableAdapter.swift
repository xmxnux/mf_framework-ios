//
//  TableAdapter.swift
//  MxnuFramework
//
//  Created by Manuel on 9/04/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class TableAdapter: NSObject, UITableViewDelegate, UITableViewDataSource{
    var customAdapter: CustomAdapter?
    var name: String?
    let COLUMN_NAME = "COLUMN_NAME"
    let WIDTH = "WIDTH"
    let DEFAULT_WIDTH: CGFloat = 200
    var dataContainer: [JsonDataContainer]
    var heights: [CGFloat]
    override init() {
        dataContainer = []
        heights = []
    }
    init(customAdapter: CustomAdapter?, name: String) {
        dataContainer = []
        heights = []
        self.customAdapter = customAdapter
        self.name = name
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataContainer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_table", for: indexPath)
        //let text = String(data[indexPath.row)
        //let text = String("data")
        //cell.textLabel?.text = text
        
        let current = dataContainer[indexPath.row]
        var view_ant: UIView? = nil
        for i in cell.contentView.subviews{
            i.removeFromSuperview()
        }
        //var maxHeight: CGFloat = 0
        var vertical_position = 0
        for i in current.getData(){
            let column = UILabelPadding()
            column.numberOfLines = 0
            column.translatesAutoresizingMaskIntoConstraints = false
            column.text = i[COLUMN_NAME] as? String
            column.lineBreakMode = .byWordWrapping
            //column.sizeToFit()
            //print("height = \(column.frame.height)")
            /*if maxHeight < column.frame.height{
                maxHeight = column.frame.height
            }*/
            cell.contentView.addSubview(column)
            
            column.layer.borderColor = UIColor.init(hexString: "#e8e8e8").cgColor
            column.layer.borderWidth = CGFloat(2)
            column.widthAnchor.constraint(equalToConstant: CGFloat(i[WIDTH] as! Int - 16)).isActive = true
            column.heightAnchor.constraint(equalToConstant: heights[indexPath.row] + 8).isActive = true
            if view_ant != nil{
                column.leftAnchor.constraint(equalTo: view_ant!.rightAnchor, constant: CGFloat(16)).isActive = true
            }
            if customAdapter != nil{
                customAdapter?.onBind(name: self.name!, holder: column,position: vertical_position, type: CustomAdapterEnum.TABLE | CustomAdapterEnum.VERTICAL)
                customAdapter?.onBind(name: self.name!, holder: column,position: indexPath.row, type: CustomAdapterEnum.TABLE | CustomAdapterEnum.HORIZONTAL)
            }
            vertical_position += 1
            view_ant = column
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heights[indexPath.row] + 16
    }
    
    func updateList(list: [JsonDataContainer], heights: [CGFloat]){
        dataContainer = list
        self.heights = heights
    }
}
class UILabelPadding: UILabel {
    
    let padding = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, padding))
    }
    
    override var intrinsicContentSize : CGSize {
        let superContentSize = super.intrinsicContentSize
        let width = superContentSize.width + padding.left + padding.right
        let heigth = superContentSize.height + padding.top + padding.bottom
        return CGSize(width: width, height: heigth)
    }
    
    
    
}
