//
//  ComboAdapter.swift
//  MxnuFramework
//
//  Created by Manuel on 13/05/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class ComboAdapter: NSObject, UIPickerViewDataSource, UIPickerViewDelegate{
    private var dataContainer: JsonDataContainer = JsonDataContainer()
    var jsonEvents: [Dictionary<String, AnyObject>]?
    var componentBase: ComponentBase?
    var selectedItem: String?
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataContainer.getCount()
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let name = dataContainer.getData()[row][dataContainer._fieldName!] as? String{
            return name
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        actionsOnSelect(row: row)
    }
    func actionsOnSelect(row: Int){
        if dataContainer.getCount() > row{
            selectedItem = dataContainer.getData()[row][dataContainer._fieldCode!] as? String
        }
        if jsonEvents != nil{
            for i in jsonEvents!{
                if let action = i["action"] as? String{
                    MF.doAction(componentBase: componentBase!, action: action, actions: i)
                }
            }
        }
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            //pickerLabel?.font = UIFont(name: "<Your Font Name>", size: <Font Size>)
            pickerLabel?.textAlignment = .center
            pickerLabel?.adjustsFontSizeToFitWidth = true
        }
        if let name = dataContainer.getData()[row][dataContainer._fieldName!] as? String{
            pickerLabel?.text = name
        }
        var texto = ""
        let fields = dataContainer.getFields()
        for det in dataContainer.getData()[row]{
            if fields[det.key] != nil{
                if texto.count != 0{
                    texto = "\(texto) \(det.value)"
                }
                else{
                    texto = "\(det.value)"
                }
            }
        }
        pickerLabel?.text = texto
        //pickerLabel?.textColor = UIColor.blue
        
        return pickerLabel!
    }
    func setDataContainer(dataContainer: JsonDataContainer){
        self.dataContainer = dataContainer
    }
    
    func updateList(list: [Dictionary<String, AnyObject>]){
        dataContainer.setData(data: list)
    }
}
