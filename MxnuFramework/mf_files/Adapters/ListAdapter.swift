//
//  ListAdapter.swift
//  MxnuFramework
//
//  Created by Manuel on 26/03/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class ListAdapter: NSObject, UITableViewDelegate, UITableViewDataSource{
    private var dataContainer: JsonDataContainer
    //var data = [Int]()
    var selectedRow = -1
    var selectedHeight: CGFloat = 45
    
    override init() {
        self.dataContainer = JsonDataContainer()
    }
    
    private func createItem(index: Int) -> UIView{
        let label = UILabel()
        if let name = dataContainer.getData()[index][dataContainer._fieldName!] as? String{
            label.text = name
        }
        else{
            label.text = ""
        }
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    private func createDetails(index: Int) -> UIView{
        let details = UIView()
        let texto1 = UILabel()
        let texto2 = UILabel()
        let fields = dataContainer.getFields()
        texto1.text = "Detalle"
        details.translatesAutoresizingMaskIntoConstraints = false
        texto1.translatesAutoresizingMaskIntoConstraints = false
        texto2.translatesAutoresizingMaskIntoConstraints = false
        var detailText = ""
        var lines = 0
        for det in dataContainer.getData()[index]{
            //itemDetail.setText(fields.get(key).getString("text") + ": " + item.getString(key));
            if let keyDict = fields[det.key]{
                if let text = keyDict["text"] as? String{
                    if detailText.count != 0{
                        detailText = "\(detailText)\n\(text): \(det.value)"
                    }
                    else{
                        detailText = "\(text): \(det.value)"
                    }
                    lines += 1
                }
            }
        }
        texto1.numberOfLines = lines
        texto1.text = detailText
        details.addSubview(texto1)
        texto1.topAnchor.constraint(equalTo: details.topAnchor, constant: 5).isActive = true
        texto1.leftAnchor.constraint(equalTo: details.leftAnchor, constant: 5).isActive = true
        
        return details
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedRow == indexPath.row{
            tableView.deselectRow(at: indexPath, animated: true)
            hideDetails(tableView: tableView, indexPath: indexPath)
            tableView.beginUpdates()
            tableView.endUpdates()
            return
        }
        let detail = createDetails(index: indexPath.row)
        if let contentView = tableView.cellForRow(at: indexPath)?.contentView{
            //let parent = contentView;
            contentView.subviews[1].addSubview(detail)
            //contentView.addSubview(detail)
            detail.translatesAutoresizingMaskIntoConstraints = false
            selectedHeight = 30 * 2
            if let detailsView = detail.subviews[0] as? UILabel{
                //print("lines = \(detailsView.numberOfLines)")
                selectedHeight = 40 * CGFloat(detailsView.numberOfLines)
            }
            selectedRow = indexPath.row
            tableView.beginUpdates()
            tableView.endUpdates()
            detail.topAnchor.constraint(equalTo: contentView.subviews[0].bottomAnchor).isActive = true
            //detail.leftAnchor.constraint(equalTo: tableView.leftAnchor, constant: 30).isActive = true
        }
    }
    
    private func hideDetails(tableView: UITableView, indexPath: IndexPath){
        let sub = tableView.cellForRow(at: indexPath)!.contentView.subviews[1]
        for i in sub.subviews{
            i.removeFromSuperview()
        }
        self.selectedRow = -1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == selectedRow{
            return self.selectedHeight
        }
        else{
            return 45
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath){
        hideDetails(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataContainer.getCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_list", for: indexPath)
        for sub in cell.contentView.subviews{
            sub.removeFromSuperview()
        }
        //let text = String(data[indexPath.row])
        //cell.textLabel?.text = text
        let sub = createItem(index: indexPath.row)
        cell.contentView.addSubview(sub)
        sub.topAnchor.constraint(equalTo: cell.contentView.topAnchor).isActive = true
        sub.leftAnchor.constraint(equalTo: cell.contentView.leftAnchor, constant: 7).isActive = true
        let sub2 = UIView()
        sub2.translatesAutoresizingMaskIntoConstraints = false
        cell.contentView.addSubview(sub2)
        sub2.topAnchor.constraint(equalTo: sub.bottomAnchor).isActive = true
        return cell
    }
    
    func setDataContainer(dataContainer: JsonDataContainer){
        self.dataContainer = dataContainer
    }
    
    func updateList(list: [Dictionary<String, AnyObject>], tableViewInstance: UITableView){
        dataContainer.setData(data: list)
        tableViewInstance.reloadData()
        tableViewInstance.refreshControl?.endRefreshing()
        
    }
}
