//
//  MxnuFrameworkOne.swift
//  MxnuFramework
//
//  Created by Manuel on 13/02/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
import UIKit
class MxnuFramework: UIViewController{
    public static var sharedView: ComponentBase?
    var callback: OnCallbackClick?
    var customAdapter: CustomAdapter?
    
    var jsonViewDict: Dictionary<String, AnyObject>
    var rootView: ComponentBase?
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        jsonViewDict = Dictionary<String, AnyObject>()
        rootView = nil
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    required init?(coder aDecoder: NSCoder) {
        jsonViewDict = Dictionary<String, AnyObject>()
        rootView = nil
        super.init(coder: aDecoder)
    }
    func setOnCallbackClick(onCallbackClick: OnCallbackClick){
        self.callback = onCallbackClick
    }
    func setCustomAdapter(customAdapter: CustomAdapter){
        self.customAdapter = customAdapter
    }
    func loadView(jsonViewStr: String){
        print("loadView from jsonViewStr")
        jsonViewDict = Dictionary<String, AnyObject>()
        let json = try! JSONSerialization.jsonObject(with: jsonViewStr.data(using: String.Encoding.utf8)!, options: .mutableLeaves)
        if let jsonDict = json as? Dictionary<String, AnyObject>{
            jsonViewDict = jsonDict
        }
        render()
    }
    func loadView(path: String){
        /*if let jsonViewStr = Util.readFile(path: "\(path).json"){
            loadView(jsonViewStr: jsonViewStr)
        }*/
        if let viewDict = Sessions.read(key: "views"), let jsonViewStr = viewDict["\(path).json"] as? String{
            loadView(jsonViewStr: jsonViewStr)
        }
        else if let path = Bundle.main.path(forResource: path, ofType: "json")
        {
            print("loadView")
            do{
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>{
                    jsonViewDict = jsonResult
                    render()
                }
                //print(self.jsonViewDict)
            }
            catch{
                
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.rootView?.viewDidAppear()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    private func render(){
        if let mf_type = self.jsonViewDict["mf-type"] as? String{
            print("mf_type: \(mf_type)")
            if let view: ComponentBase = ObjectResolver.getInstance(className: mf_type, context: self.view, jsonView: self.jsonViewDict, onCallackClick: callback, customAdapter: customAdapter) as! ComponentBase?, let viewCreated = view.createView() {
                print("pendiente de uso = \(viewCreated)")
                //print("view not null")
                self.rootView = view
                //self.view.addSubview(viewCreated)
                //viewCreated.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
                //viewCreated.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
                let cb = ComponentBase(context: self.view, jsonView: Dictionary<String, AnyObject>(), onCallackClick: callback, customAdapter: customAdapter)
                cb.view = self.view
                view.setParent(parent: cb)
                view.setupLayout()
                MxnuFramework.sharedView = view
                //viewCreated.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
            }
        }
    }
}
extension UIColor {
    convenience init(hexString : String)
    {
        let hexString = hexString[hexString.index(hexString.startIndex, offsetBy: 1)...]
        if let rgbValue = UInt(hexString, radix: 16) {
            let red   =  CGFloat((rgbValue >> 16) & 0xff) / 255
            let green =  CGFloat((rgbValue >>  8) & 0xff) / 255
            let blue  =  CGFloat((rgbValue      ) & 0xff) / 255
            self.init(red: red, green: green, blue: blue, alpha: 1.0)
        } else {
            self.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        }
    }
}
