//
//  JsonDataContainer.swift
//  MxnuFramework
//
//  Created by Manuel on 9/04/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
class JsonDataContainer{
    var fields: Dictionary<String, Dictionary<String, AnyObject>> = Dictionary<String, Dictionary<String, AnyObject>>()
    var data: [Dictionary<String, AnyObject>] = []
    var _fieldCode: String?
    var _fieldName: String?
    var title: String?
    
    func setFields(fields: [Dictionary<String, AnyObject>]){
        self.fields.removeAll()
        for i in fields{
            if let name = i["dataIndex"] as? String{
                self.fields[name] = i
            }
        }
    }
    
    func getFields() -> Dictionary<String, Dictionary<String, AnyObject>>{
        return fields
    }
    
    func getData() -> [Dictionary<String, AnyObject>]{
        return data
    }
    
    func setData(data: [Dictionary<String, AnyObject>]){
        self.data = data
    }
    
    func getTitle() -> String?{
        return self.title
    }
    
    func setTitle(title: String){
        self.title = title
    }
    
    func getCount() -> Int{
        return self.data.count
    }
    
    func setFieldCode(code: String){
        self._fieldCode = code
    }
    
    func setFieldName(name: String){
        self._fieldName = name
    }
}
