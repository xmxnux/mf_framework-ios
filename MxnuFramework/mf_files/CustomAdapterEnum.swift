//
//  CustomAdapterEnum.swift
//  MxnuFramework
//
//  Created by Manuel on 15/05/18.
//  Copyright © 2018 Manuel. All rights reserved.
//

import Foundation
class CustomAdapterEnum{
    static let TABLE = 0;
    static let LIST = 1;
    static let VERTICAL = 2;
    static let HORIZONTAL = 4;
}
